package centers;

import cars.VolkswagenCar;

import javax.security.auth.login.LoginException;

//Service where you can buy a car
public abstract class VolkswagenAG {

    public VolkswagenCar orderCar(Class<? extends VolkswagenCar> model) throws LoginException {
        VolkswagenCar volkswagenCar = sellCar(model);
        volkswagenCar.deliverCar();
        return volkswagenCar;
    }

    protected abstract VolkswagenCar sellCar(Class<? extends VolkswagenCar> model) throws LoginException;
}
