package centers;

import cars.VolkswagenCar;
import cars.volkswagen.Golf;
import cars.volkswagen.Polo;
import cars.volkswagen.Touareg;

import javax.security.auth.login.LoginException;

public class VolkswagenCenter extends VolkswagenAG {

    @Override
    protected VolkswagenCar sellCar(Class<? extends VolkswagenCar> model) throws LoginException {
        if(model == Golf.class){
            return new Golf();
        }

        if(model == Touareg.class){
            return new Touareg();
        }

        if(model == Polo.class){
            return new Polo();
        }

        throw new LoginException("No such car in this center, try another one");
    }
}
