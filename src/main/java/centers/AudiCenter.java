package centers;

import cars.VolkswagenCar;
import cars.audi.A4;
import cars.audi.Q8;
import cars.audi.RS5;

import javax.security.auth.login.LoginException;

public class AudiCenter extends VolkswagenAG{

    @Override
    protected VolkswagenCar sellCar(Class<? extends VolkswagenCar> model) throws LoginException {
        if(model == A4.class){
            return new A4();
        }

        if(model == Q8.class){
            return new Q8();
        }

        if(model == RS5.class){
            return new RS5();
        }

        throw new LoginException("No such car in this center, try another one");
    }
}
