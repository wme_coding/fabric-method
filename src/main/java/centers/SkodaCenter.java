package centers;

import cars.VolkswagenCar;
import cars.skoda.Karoq;
import cars.skoda.Oktavia;
import cars.skoda.Rapid;

import javax.security.auth.login.LoginException;

public class SkodaCenter extends VolkswagenAG {

    @Override
    protected VolkswagenCar sellCar(Class<? extends VolkswagenCar> model) throws LoginException {
        if(model == Oktavia.class){
            return new Oktavia();
        }

        if(model == Rapid.class){
            return new Rapid();
        }

        if(model == Karoq.class){
            return new Karoq();
        }

        throw new LoginException("No such car in this center, try another one");
    }
}
