package centers;

import cars.VolkswagenCar;
import cars.porche.Cayenne;
import cars.porche.Macan;
import cars.porche.Panamera;

import javax.security.auth.login.LoginException;

public class PorcheCenter extends VolkswagenAG {
    @Override
    protected VolkswagenCar sellCar(Class<? extends VolkswagenCar> model) throws LoginException {
        if(model == Panamera.class){
            return new Panamera();
        }

        if(model == Macan.class){
            return new Macan();
        }

        if(model == Cayenne.class){
            return new Cayenne();
        }

        throw new LoginException("No such car in this center, try another one");
    }
}
