import cars.VolkswagenCar;
import cars.audi.A4;
import cars.porche.Panamera;
import cars.volkswagen.Polo;
import centers.AudiCenter;
import centers.PorcheCenter;
import centers.VolkswagenAG;
import centers.VolkswagenCenter;
import org.junit.Assert;
import org.junit.Test;

import javax.security.auth.login.LoginException;

public class TestBuyingCar {
    @Test
    public void testAudiCenter() throws LoginException {
        VolkswagenAG audiCenter = new AudiCenter();
        VolkswagenCar a4 = audiCenter.orderCar(A4.class);
        Assert.assertEquals(A4.class, a4.getClass());
    }

    @Test
    public void testVolkswagenCenter() throws LoginException {
        VolkswagenAG volkswagenCenter = new VolkswagenCenter();
        VolkswagenCar polo = volkswagenCenter.orderCar(Polo.class);
        Assert.assertEquals(Polo.class, polo.getClass());
    }

    @Test
    public void testPorcheCenter() throws LoginException {
        VolkswagenAG porcheCenter = new PorcheCenter();
        VolkswagenCar panamera = porcheCenter.orderCar(Panamera.class);
        Assert.assertEquals(Panamera.class, panamera.getClass());
    }

    @Test(expected = LoginException.class)
    public void testWrongModelException() throws LoginException {
        VolkswagenAG porcheCenter = new PorcheCenter();
        VolkswagenCar polo = porcheCenter.orderCar(Polo.class);
    }
}
